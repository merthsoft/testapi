﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NoFaceBookApi.Models {
    public class Post {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Subject { get; set; }
        public string Contents { get; set; }
        public DateTime TimeStamp { get; set; }

        public Post() {
            Id = new Guid();
        }
    }
}
