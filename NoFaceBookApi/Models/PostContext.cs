﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NoFaceBookApi.Models {
    public class PostContext : DbContext {
        public DbSet<Post> Posts { get; set; }

        public PostContext() : base() { }
        public PostContext(DbContextOptions<PostContext> options) : base(options) { }
                
    }
}
