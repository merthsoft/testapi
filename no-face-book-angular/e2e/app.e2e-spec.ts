import { NoFaceBookAngularPage } from './app.po';

describe('no-face-book-angular App', function() {
  let page: NoFaceBookAngularPage;

  beforeEach(() => {
    page = new NoFaceBookAngularPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
