import { Component } from '@angular/core';
import { Post } from "./models/post.model";
import { Http, RequestOptions, Headers, URLSearchParams, Response} from '@angular/http'
import "rxjs/add/operator/map";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private apiUrl = 'https://localhost:44378/api/posts/';
  private posts: Array<Post> = [];
  private newPost: Post = new Post();
  private contents: Array<string> = [];

  constructor(private http: Http) {
    this.getPosts();
  }

  postNewMessage() {
    this.http.post(this.apiUrl, this.newPost).map((res: Response) => res.json()).subscribe(post => {
      this.posts.push(post);
      this.cacheContents(post);
      this.newPost = new Post();
    })
  }

  update(id: string) {
    var text = '"' + this.contents[id] + '"';

    let opt: RequestOptions
    let headers: Headers = new Headers
    let searchParams: URLSearchParams = new URLSearchParams()

    headers.set('Content-type', 'application/json')
    opt = new RequestOptions({
      search: searchParams,
      headers: headers
    })

    this.http.put(this.apiUrl + id, text, opt).map((res: Response) => res.json()).subscribe(post => {
      var index = this.posts.findIndex(function match(p) { return p.id === post.id });
      this.posts[index] = post;
    })
  }

  delete(id: string) {
    this.http.delete(this.apiUrl + id).map((res: Response) => res.json()).subscribe(post => {
      var index = this.posts.findIndex(function match(p) { return p.id === post.id });
      this.posts.splice(index, 1);
    })
  }

  cacheContents(post) {
    this.contents[post.id] = post.contents;
  }

  getPosts() {
    this.http.get(this.apiUrl).map((res: Response) => res.json()).subscribe(data => {
      this.posts = data;
      for (let post of data) {
        this.cacheContents(post);
      }
    })
  }
}
