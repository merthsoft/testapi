export class Post {
  id: string;
  userName: string;
  subject: string;
  contents: string;
  timeStamp: Date;
}
